/* This file is dedicated to the public domain. */

/*
 * #includes {{{ */

#define _POSIX_SYNCHRONIZED_IO // ... excuse me??!
#include <dirent.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <pwd.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

/* }}}
 */

/*
 * inlined option parser (opt.h) {{{ */

static char *_argv0;
static char *_usage;
static void usagex(int ret) {
	fprintf(stderr, "usage: %s %s\n", _argv0, _usage);
	exit(ret);
}
#define usage() usagex(1)
#define USAGE(s) static char *_usage = "" s

#define FOR_OPTS(argc, argv, CODE) do { \
	--(argc); \
	_argv0 = *(argv)++; \
	for (char *_p = *(argv), *_p1; *(argv); _p = *++(argv), --(argc)) { \
		(void)(_p1); /* avoid unused warnings if OPTARG isn't used */ \
		if (*_p != '-' || !*++_p) break; \
		if (*_p == '-' && !*(_p + 1)) { ++(argv); --(argc); break; } \
		while (*_p) { \
			switch (*_p++) { \
				default: warnx("invalid option: %c", *(_p - 1)); usage(); \
				CODE \
			} \
		} \
	} \
} while(0)

#define OPTARG(argc, argv) \
	(*_p ? (_p1 = _p, _p = "", _p1) : (*(--(argc), ++(argv)) ? *(argv) : \
		((warnx("missing argument for option -%c", *(_p - 1))), usage(), \
			(char *)0)))

/* }}}
 */

/*
 * inlined dynamic vector (vec.h) {{{ */

struct _vec {
	uint32_t sz;
	uint32_t max;
	void *data;
};

/*
 * Usage: struct VEC(my_type) myvec = {0};
 */
#define VEC(type) { \
	uint32_t sz; \
	uint32_t max; \
	type *data; \
}

#ifdef __GNUC__
__attribute__((unused)) // heck off gcc
#endif
static bool _vec_make_room(struct _vec *v, uint32_t tsize, uint32_t newcnt) {
	uint32_t x = v->max + newcnt;
	if (x < 16) {
		x = 16;
	}
	else {
		// round up to next 2*n
		--x;
		x |= x >> 1; x |= x >> 2; x |= x >> 4; x |= x >> 8; x |= x >> 16;
		x++;
	}

	void *new = realloc(v->data, x * tsize); // **assume** no overflow
	if (new) {
		v->data = new;
		v->max = x;
	}
	return !!new;
}

#define vec_push(v, val) ( \
	((v)->sz < (v)->max || \
	_vec_make_room((struct _vec *)(v), sizeof(val), 1)) && \
	((v)->data[(v)->sz++] = (val), true) \
)

#define vec_pop(v) ((v)->data[--(v)->sz])

/* }}}
 */

/*
 * miscellaneous utility stuff {{{ */

struct str VEC(char);

bool str_append(struct str *s, char *new) {
	uint32_t backtrack = s->sz;
	--(s->sz);
	for (; *new; ++new) {
		if (!vec_push(s, *new)) {
			s->sz = backtrack;
			s->data[s->sz - 1] = '\0';
			return false;
		}
	}
	if (!vec_push(s, '\0')) {
		s->sz = backtrack;
		s->data[s->sz - 1] = '\0';
		return false;
	}
	return true;
}

/* }}}
 */

/* - - the program itself - - - */

USAGE("[-us] pkgname [fromdir]");

#define RECURSION_DEPTH 64

// NOTE: this is used in install_file() and uninstall(), which are called
// separately. It probably shouldn't be used anywhere else!
static char buf[65536];

static const char *localdirname = "dotlocal";

static int open_usrlocal(void) {
	int fd = open("/usr/local", O_RDONLY | O_DIRECTORY);
	if (fd == -1) err(1, "open(/usr/local)");
	return fd;
}

static int open_dotlocal(void) {
	const char *path = getenv("XDG_LOCAL_DIR");
	if (path) {
		int fd = open(path, O_RDONLY | O_DIRECTORY);
		if (fd == -1) err(1, "open(%s)", path);
		return fd;
	}
	else {
		char *home = getenv("HOME");
		if (!home) {
			struct passwd *pw = getpwuid(getuid());
			if (!pw) err(1, "getpwuid");
			home = pw->pw_dir;
		}
		int hfd = open(home, O_RDONLY | O_DIRECTORY);
		if (hfd == -1) err(1, "open(%s)", home);
		int fd = openat(hfd, ".local", O_RDONLY | O_DIRECTORY);
		if (fd == -1) err(1, "open(%s/%s)", home, ".local");
		if (dup2(fd, hfd) == -1) err(1, "dup2");
		return hfd;
	}
}

static int open_lpkgdir(int dotlocal, bool create) {
	int fd = openat(dotlocal, "share/lpkg", O_RDONLY | O_DIRECTORY);
	if (fd == -1) {
		if (!create || errno != ENOENT) {
			err(1, "open(%s/share/lpkg)", localdirname);
		}
		if (mkdirat(dotlocal, "share/lpkg", 0755) == -1) {
			err(1, "mkdir(%s/share/lpkg)", localdirname);
		}
		fd = openat(dotlocal, "share/lpkg", O_RDONLY | O_DIRECTORY);
		if (fd == -1) err(1, "open(%s/share/lpkg)", localdirname);
		return fd;
	}
	return fd;
}

static int open_pkgdef(int lpkgdir, const char *name, bool create) {
	int fd = create ?
		openat(lpkgdir, name, O_RDWR | O_CREAT | O_TRUNC, 0644) :
		openat(lpkgdir, name, O_RDWR);
	if (fd == -1) err(1, "open(%s/share/lpkg/%s)", localdirname, name);
	return fd;
}

static int open_fromdir(const char *fromdir) {
	int fd = open(fromdir, O_RDONLY | O_DIRECTORY);
	if (fd == -1) err(1, "open(%s)", fromdir);
	return fd;
}

static void checkempty(int dir, const char *name) {
	DIR *d = fdopendir(dir);
	if (!d) { warn("warning: fdopendir"); return; }
	readdir(d); readdir(d); // skip . and ..
	if (!readdir(d)) {
		warnx("note: leaving behind empty directory %s, "
				"remove manually if desired", name);
	}
	closedir(d);
}

static void uninstall(int pkgdef, int dotlocal) {
	ssize_t n;
	char pathstr[PATH_MAX];
	char *pathp = pathstr;
	char *lastslash = 0;
	while ((n = read(pkgdef, buf, sizeof(buf))) > 0) {
		for (char *p = buf; p - buf < n; ++p) {
			if (pathp - pathstr == sizeof(pathstr)) {
				errx(1, "unreasonably long path in package definition");
			}
			*pathp++ = *p;
			if (*p == '/') {
				lastslash = pathp;
			}
			else if (!*p) { // it's a whole null-terminated path
				if (unlinkat(dotlocal, pathstr, 0) == -1) {
					err(1, "unlink(%s/%s)", localdirname, pathstr);
				}
				if (lastslash) {
					*lastslash = '\0';
					// XXX a bit inefficient to opendir every time we delete a
					// file - it would be cooler to make a list of dirs to check
					// afterwards, but can't be bothered doing that
					int dirfd = openat(dotlocal, pathstr,
							O_RDONLY | O_DIRECTORY);
					if (dirfd == -1) {
						warn("warning: open(%s/%s)", localdirname, pathstr);
					}
					else {
						checkempty(dirfd, pathstr);
					}
				}
				pathp = pathstr;
				lastslash = 0;
			}
		}
	}
	if (n == -1) err(1, "read(pkgdef)");
}

static void unlink_pkgdef(int lpkgdir, const char *name) {
	if (unlinkat(lpkgdir, name, 0) == -1) {
		err(1, "unlinkat(%s/share/lpkg/%s", localdirname, name);
	}
}

static void install_file(int pkgdef, int tofile, int fromfile,
		struct str *relpath) {
	ssize_t n;
	while ((n = read(fromfile, buf, sizeof(buf))) > 0) {
		size_t m;
		size_t t = 0;
		// XXX these write loops are a bit wonkily written but they work for now
		while (t < n && (m = write(tofile, buf + t, n - t)) > 0) t += m;
		if (m == -1) err(1, "write(%s)", relpath->data);
	}
	if (n == -1) err(1, "read(%s)", relpath->data);
	if (fsync(tofile) == -1) err(1, "fsync(%s)", relpath->data);

	// write the paths out including the null terminators
	size_t m;
	size_t t = 0;
	while (t < relpath->sz && (m = write(pkgdef, relpath->data + t,
			relpath->sz - t)) > 0) {
		t += m;
	}
	if (m == -1) err(1, "write(pkgdef)");

	close(tofile);
	close(fromfile);
}

static void install_dir(int pkgdef, int todir, int fromdir, int recursion,
		struct str *relpath) {
	if (!recursion) errx(1, "recursion depth exceeded");

	DIR *d = fdopendir(fromdir);
	if (!d) err(1, "fdopendir");
	struct dirent *e;
	while (e = readdir(d)) {
		// skip . and ..
		if (e->d_name[0] == '.' && (e->d_name[1] == '\0' ||
				e->d_name[1] == '.' && e->d_name[2] == '\0')) {
			continue;
		}
		int fromfd = openat(fromdir, e->d_name, O_RDONLY);
		if (fromfd == -1) err(1, "openat(%s)", e->d_name);
		struct stat st;
		if (fstat(fromfd, &st) == -1) err(1, "stat(%s)", e->d_name);
		uint32_t backtrack = relpath->sz;
		// don't put a slash at the very front
		if (relpath->sz > 1) {
			relpath->data[backtrack - 1] = '/';
			if (!vec_push(relpath, '\0')) err(1, "vec_push(relpath)");
		}
		if (!str_append(relpath, e->d_name)) err(1, "str_append(relpath)");
		if (S_ISDIR(st.st_mode)) {
			// NOTE: for now, making intermediate directories, but never
			// assigning them to packages nor removing them.
			// Can't think of a way to do it different that's also fairly
			// simple, plus this doesn't reaallly matter that much.
			if (mkdirat(todir, e->d_name, st.st_mode) == -1 &&
					errno != EEXIST) {
				err(1, "mkdirat(%s)", e->d_name);
			}
			int tofd = openat(todir, e->d_name, O_RDONLY | O_DIRECTORY);
			if (tofd == -1) err(1, "openat(%s)", e->d_name);
			install_dir(pkgdef, tofd, fromfd, recursion - 1, relpath);
		}
		else {
			int tofd = openat(todir, e->d_name, O_RDWR | O_CREAT | O_TRUNC,
					st.st_mode);
			if (tofd == -1) err(1, "openat(%s)", e->d_name);
			install_file(pkgdef, tofd, fromfd, relpath);
		}
		relpath->sz = backtrack;
	}
	closedir(d);
}

static void install(int pkgdef, int dotlocal, int fromdir) {
	struct str relpath = {0};
	if (!vec_push(&relpath, '\0')) err(1, "vec_push");
	install_dir(pkgdef, dotlocal, fromdir, RECURSION_DEPTH, &relpath);
}

static void startuninstall(const char *name, bool syswide) {
	int dotlocal = syswide ? open_usrlocal() : open_dotlocal();
	int lpkgdir = open_lpkgdir(dotlocal, false);
	int pkgdef = open_pkgdef(lpkgdir, name, false);
	uninstall(pkgdef, dotlocal);
	unlink_pkgdef(lpkgdir, name);
}

static void startinstall(const char *name, const char *fromdir, bool syswide) {
	int dotlocal = syswide ? open_usrlocal() : open_dotlocal();
	int lpkgdir = open_lpkgdir(dotlocal, true);
	int pkgdef = open_pkgdef(lpkgdir, name, true);
	int fromdirfd = open_fromdir(fromdir);
	uninstall(pkgdef, dotlocal);
	lseek(pkgdef, 0, SEEK_SET);
	if (ftruncate(pkgdef, 0) == -1) {
		err(1, "ftruncate(%s/share/lpkg/%s)", localdirname, name);
	}
	install(pkgdef, dotlocal, fromdirfd);
	if (fsync(pkgdef) == -1) {
		err(1, "fsync(%s/share/lpkg/%s)", localdirname, name);
	}
}

static void dolist(bool syswide) {
	int dotlocal = syswide ? open_usrlocal() : open_dotlocal();
	int lpkgdir = open_lpkgdir(dotlocal, false);
	DIR *d = fdopendir(lpkgdir);
	struct dirent *e;
	while (e = readdir(d)) {
		if (e->d_name[0] == '.' && (e->d_name[1] == '\0' ||
				e->d_name[1] == '.' && e->d_name[2] == '\0')) {
			continue;
		}
		puts(e->d_name);
	}
}

int main(int argc, char *argv[]) {
	enum { install, uninstall, list } mode = install;
	bool syswide = false;
	FOR_OPTS(argc, argv, {
		case 's': syswide = true; break;
		case 'u': mode = uninstall; break;
		case 'l': mode = list;
	});
	
	if (syswide) localdirname = "/usr/local";

	if (mode == install) {
		switch (argc) {
			// disambiguate the USAGE() string
			case 1: warnx("directory name not specified");
			default: usage();
			case 2: startinstall(argv[0], argv[1], syswide);
		}
	}
	else if (mode == uninstall) {
		switch (argc) {
			// also disambiguate the USAGE() string
			case 2: warnx("unexpected argument given");
			default: usage();
			case 1: startuninstall(argv[0], syswide);
		}
	}
	else /* mode == list */ {
		if (argc) {
			warnx("unexpected argument given");
			usage();
		}
		dolist(syswide);
	}

	return 0;
}

// vi: sw=4 ts=4 noet tw=80 cc=80 fdm=marker
